Set show git informations (colorized) current direcotory on the command prompt* like this:

```
USER@HOST:/PATH/TO/WORKDIR:[GIT_BRANCH]# 
```
### Informations

* 0.0.1
* [Learn git](https://git-scm.com/docs/)
* [Learn Bash Prompt](http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/)
* *(only commons Linux OS distributions tested)

### Set up
```
#cd PATH/TO/WORKSPACE
#export PS1='\[\033[0;32m\]\[\033[0m\033[0;32m\]\u@\h:\[\033[0;36m\]\w\:\033[0;31m\][$(git branch 2>/dev/null | grep "^*" | colrm 1 2)\[\033[0;31m\]]\[\033[0m\033[0;32m\]#\[\033[0m\033[0;32m\]\[\033[0m\]'
```